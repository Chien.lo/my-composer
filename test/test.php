<?php
require_once "../vendor/autoload.php";

$obj = new \my-composer\hello\HelloWorld();
echo $obj->hello();

echo "\n";
$obj = new \my-composer\hello\HelloWorld('My Goddess');
echo $obj->hello();
